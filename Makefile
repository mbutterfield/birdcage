build:
	go build -o bin/hub hub.go
	go build -o bin/thermostat thermostat.go

fmt:
	@gofmt -l -s -w $(shell find . -type f -name '*.go' -not -path "./vendor/*")

test:
	go test -v ./hub/...
	go test -v ./thermostat/...

vet:
	go vet -n $(shell find . -type f -name '*.go' -maxdepth 1)
	go vet -n ./hub/...
	go vet -n ./thermostat/...
