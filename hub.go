package main

import (
	"bitbucket.org/mbutterfield/birdcage/hub"
	"fmt"
)

func main() {
	app := hub.NewApp()
	fmt.Println("listening on port 3000")
	if err := app.Run(3000); err != nil {
		panic(err)
	}
}
