package thermostat

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetThermostat(t *testing.T) {
	app := NewApp(1234, "living room", 0, 0)
	r, err := http.NewRequest(http.MethodGet, "/thermostat", nil)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()
	app.router.ServeHTTP(w, r)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Fatalf("Unexpected return code: %d, with body: %s", w.Code, body)
	}
	var thermostat *Thermostat
	if err := json.Unmarshal(body, &thermostat); err != nil {
		t.Fatal(err)
	}

	if *thermostat != *app.Thermostat {
		t.Errorf("Unexpected response: %v != %v", thermostat, app.Thermostat)
	}
}

func TestUpdateThermostat(t *testing.T) {
	app := NewApp(1234, "living room", 70, 0)
	changes := NewThermostat(1234, "living room", 75, 0)
	changes.ID = app.Thermostat.ID
	result, err := json.Marshal(changes)
	if err != nil {
		t.Fatal(err)
	}
	r, err := http.NewRequest(http.MethodPut, "/thermostat", bytes.NewBuffer(result))
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()
	app.router.ServeHTTP(w, r)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Fatalf("Unexpected return code: %d, with body: %s", w.Code, body)
	}
	var thermostat *Thermostat
	if err := json.Unmarshal(body, &thermostat); err != nil {
		t.Fatal(err)
	}

	if *thermostat != *changes {
		t.Errorf("result does not match changes: %v != %v", thermostat, changes)
	}
	if *thermostat != *app.Thermostat {
		t.Errorf("result does not equal app.Thermostat: %v != %v", thermostat, app.Thermostat)
	}
}
