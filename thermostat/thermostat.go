package thermostat

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/twinj/uuid"
	"io/ioutil"
	"net/http"
)

const (
	OnlineStatus  = "online"
	OfflineStatus = "offline"
)

type Thermostat struct {
	ID                 string `json:"id"`
	IPAddress          string `json:"ip_address"`
	Port               uint   `json:"port"`
	OnlineStatus       string `json:"online_status"`
	CurrentTemperature uint   `json:"current_temperature"`
	TargetTemperature  uint   `json:"target_temperature"`
	Nickname           string `json:"nickname"`
}

func NewThermostat(port uint, nickname string, target_temp, current_temp uint) *Thermostat {
	return &Thermostat{
		ID:                 uuid.NewV4().String(),
		IPAddress:          "127.0.0.1",
		Port:               port,
		OnlineStatus:       OnlineStatus,
		Nickname:           nickname,
		TargetTemperature:  target_temp,
		CurrentTemperature: current_temp,
	}
}

type App struct {
	router     *mux.Router
	Thermostat *Thermostat
}

func NewApp(port uint, nickname string, target_temp, current_temp uint) *App {
	app := &App{
		router:     mux.NewRouter(),
		Thermostat: NewThermostat(port, nickname, target_temp, current_temp),
	}
	app.router.HandleFunc("/thermostat", app.handler).Methods(http.MethodGet, http.MethodPut)
	return app
}

func (a *App) Run() error {
	return http.ListenAndServe(fmt.Sprintf("localhost:%d", a.Thermostat.Port), a.router)
}

func (a *App) handler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		a.serializeThermostat(w)
	} else {
		a.updateThermostat(w, r)
	}
}

func (a *App) serializeThermostat(w http.ResponseWriter) {
	result, err := json.Marshal(a.Thermostat)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(result)
}

func (a *App) updateThermostat(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var changed Thermostat
	if err := json.Unmarshal(b, &changed); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if changed.TargetTemperature > 0 {
		a.Thermostat.TargetTemperature = changed.TargetTemperature
	}
	if changed.Nickname != "" {
		a.Thermostat.Nickname = changed.Nickname
	}
	a.serializeThermostat(w)
}
