Birdcage.io
===========

This project consists of two applications: `hub` and `thermostat`.
It was developed with the [go](https://golang.org/) programming language (version 1.8).

## Setup
To build the applications, make sure you have your GOPATH environment variable set, then clone this repo into it:

    mkdir -p $GOPATH/src/bitbucket.org/mbutterfield
    cd $GOPATH/src/bitbucket.org/mbutterfield
    git clone git@bitbucket.org:mbutterfield/birdcage.git

At this point you should be able to run `make build`.  This will give you two executable files in `bin`, one for the hub and one for thermostats.

For an example, run `bin/hub`.
This will start the hub application running on port 3000.
Then in another terminal run `./demo` which will start three thermostats running on ports 8001, 8002, and 8003.

At this point you can point your web browser to [http://localhost:3000/thermostat](http://localhost:3000/thermostat) to view the thermostats connected to the hub.

You should see something like this: 

```json
{  
   "data":[  
      {  
         "id":"b81c9005-144f-4d85-92a3-9fa4c0db1141",
         "ip_address":"127.0.0.1",
         "port":8002,
         "online_status":"online",
         "current_temperature":74,
         "target_temperature":71,
         "nickname":"upstairs"
      },
      {  
         "id":"d34b622d-7eb8-466a-9fc0-a3790fb4fd18",
         "ip_address":"127.0.0.1",
         "port":8003,
         "online_status":"online",
         "current_temperature":67,
         "target_temperature":71,
         "nickname":"garage"
      },
      {  
         "id":"bf41e2e1-5e79-4cec-a74b-2f6c3343249a",
         "ip_address":"127.0.0.1",
         "port":8001,
         "online_status":"online",
         "current_temperature":72,
         "target_temperature":75,
         "nickname":"living room"
      }
   ],
   "meta":{  
      "average_current_room_temperature":71
   }
}
```
As you can see, this shows all connected thermostats inside `data`, along with the average current room temperature in `meta`.

Other options:

  * To view information about a specific thermostat, make a GET request to `http://localhost:3000/thermostat/<id>` where `<id>` is the ID of the thermostat you wish to view.
  * To set the target temperature on the specific thermostat, make a PUT request to the same url with a body like:
```{"target_temperature": 71}```
  * To set the target temperature on all connected thermostats, make a PUT request to `http://localhost:3000/thermostat` with the same body.

The thermostat application has a similar API, which can be accessed at `http://localhost:<port>/thermostat`.
GET requests will return information about the thermostat, and PUT requests can be used to update the temperature or nickname of the thermostat.
