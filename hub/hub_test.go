package hub

import (
	"bitbucket.org/mbutterfield/birdcage/thermostat"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestListThermostats(t *testing.T) {
	app := NewApp()
	tstat1 := thermostat.NewThermostat(1234, "living room", 50, 100)
	tstat2 := thermostat.NewThermostat(4567, "hallway", 100, 50)
	app.thermostatMap[tstat1.ID] = tstat1
	app.thermostatMap[tstat2.ID] = tstat2
	r, err := http.NewRequest(http.MethodGet, "/thermostat", nil)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()
	app.router.ServeHTTP(w, r)

	result := w.Result()
	body, err := ioutil.ReadAll(result.Body)
	if err != nil {
		t.Fatal(err)
	}
	if result.StatusCode != http.StatusOK {
		t.Fatalf("Unexpected return code: %d, with body: %s", w.Code, body)
	}
	var resp *response
	if err := json.Unmarshal(body, &resp); err != nil {
		t.Fatal(err)
	}

	if len(resp.Data) != len(app.thermostatMap) {
		t.Errorf("%v != %v", resp.Data, app.thermostatMap)
	}
	for _, elem := range resp.Data {
		if _, ok := app.thermostatMap[elem.ID]; !ok {
			t.Errorf("%v not found in %v", elem, app.thermostatMap)
		}
	}
}

func TestGetThermostatInfo(t *testing.T) {
	app := NewApp()
	tstat := thermostat.NewThermostat(1234, "living room", 0, 0)
	app.thermostatMap[tstat.ID] = tstat
	r, err := http.NewRequest(http.MethodGet, "/thermostat/"+tstat.ID, nil)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()
	app.router.ServeHTTP(w, r)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Fatalf("Unexpected return code: %d, with body: %s", w.Code, body)
	}
	var result *thermostat.Thermostat
	if err := json.Unmarshal(body, &result); err != nil {
		t.Fatal(err)
	}

	if *result != *tstat {
		t.Errorf("Unexpected response: %v != %v", result, tstat)
	}
}

func TestRegisterThermostat(t *testing.T) {
	app := NewApp()
	tstat := thermostat.NewThermostat(1234, "living room", 0, 0)
	app.thermostatMap[tstat.ID] = tstat
	result, err := json.Marshal(tstat)
	if err != nil {
		t.Fatal(err)
	}
	r, err := http.NewRequest(http.MethodPost, "/thermostat", bytes.NewBuffer(result))
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()
	app.router.ServeHTTP(w, r)

	resp := w.Result()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != http.StatusCreated {
		t.Fatalf("Unexpected return code: %d, with body: %s", w.Code, body)
	}
	var tstatResult *thermostat.Thermostat
	if err := json.Unmarshal(body, &tstatResult); err != nil {
		t.Fatal(err)
	}

	if *tstatResult != *tstat {
		t.Errorf("Unexpected response: %v != %v", result, tstat)
	}
}
