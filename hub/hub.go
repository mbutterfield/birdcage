package hub

import (
	"bitbucket.org/mbutterfield/birdcage/thermostat"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"math"
	"net"
	"net/http"
	"strconv"
	"sync"
)

type response struct {
	Data []*thermostat.Thermostat `json:"data"`
	Meta map[string]interface{}   `json:"meta"`
}

type App struct {
	router        *mux.Router
	thermostatMap map[string]*thermostat.Thermostat
	mapMutex      *sync.RWMutex
}

func NewApp() *App {
	app := &App{
		router:        mux.NewRouter(),
		thermostatMap: map[string]*thermostat.Thermostat{},
		mapMutex:      &sync.RWMutex{},
	}
	app.router.HandleFunc("/thermostat", app.handleThermostats).Methods(http.MethodGet, http.MethodPut, http.MethodPost)
	app.router.HandleFunc("/thermostat/{id:[a-z0-9-]+}", app.handleThermostat).Methods(http.MethodGet, http.MethodPut)
	return app
}

func (a *App) Run(port uint) error {
	return http.ListenAndServe(fmt.Sprintf("localhost:%d", port), a.router)
}

func (a *App) handleThermostats(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		a.registerThermostat(w, r)
		return
	} else if r.Method == http.MethodPut {
		a.mapMutex.RLock()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		for _, t := range a.thermostatMap {
			if t.OnlineStatus == thermostat.OnlineStatus {
				if _, err := updateThermostat(t, body); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
			}
		}
		a.mapMutex.RUnlock()
	}
	thermostats := []*thermostat.Thermostat{}
	a.mapMutex.RLock()
	for _, t := range a.thermostatMap {
		thermostats = append(thermostats, t)
	}
	a.mapMutex.RUnlock()
	meta := map[string]interface{}{}
	if len(thermostats) > 0 {
		meta["average_current_room_temperature"] = avgRoomTemp(thermostats)
	}
	result, err := json.Marshal(response{
		Data: thermostats,
		Meta: meta,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(result)
}

func (a *App) handleThermostat(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPut {
		a.mapMutex.RLock()
		t, ok := a.thermostatMap[mux.Vars(r)["id"]]
		a.mapMutex.RUnlock()
		if !ok {
			http.Error(w, "Thermostat not found", http.StatusNotFound)
			return
		}
		reqBody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		body, err := updateThermostat(t, reqBody)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		a.mapMutex.Lock()
		a.thermostatMap[t.ID] = t
		a.mapMutex.Unlock()
		w.Header().Set("Content-Type", "application/json")
		w.Write(body)
	} else {
		a.mapMutex.RLock()
		t, ok := a.thermostatMap[mux.Vars(r)["id"]]
		a.mapMutex.RUnlock()
		if ok {
			result, err := json.Marshal(t)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Header().Set("Content-Type", "application/json")
			w.Write(result)
			return
		} else {
			http.Error(w, "Thermostat not found", http.StatusNotFound)
		}
	}
}

func updateThermostat(t *thermostat.Thermostat, reqBody []byte) ([]byte, error) {
	tr := &http.Transport{
		DisableCompression: true,
		DisableKeepAlives:  false,
	}
	client := &http.Client{Transport: tr}
	url := fmt.Sprintf("http://%s/thermostat", net.JoinHostPort(t.IPAddress, strconv.Itoa(int(t.Port))))
	req, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(reqBody))
	if err != nil {
		return nil, err
	}
	resp, err := client.Do(req)
	if err != nil {
		t.OnlineStatus = thermostat.OfflineStatus
		return nil, errors.New("Could not connect to thermostat")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(body, t); err != nil {
		return nil, err
	}
	return body, nil
}

func (a *App) registerThermostat(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var t *thermostat.Thermostat
	if err := json.Unmarshal(b, &t); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	a.mapMutex.Lock()
	a.thermostatMap[t.ID] = t
	a.mapMutex.Unlock()
	result, err := json.Marshal(t)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(result)
}

func avgRoomTemp(thermostats []*thermostat.Thermostat) int {
	total := 0.0
	for _, t := range thermostats {
		total += float64(t.CurrentTemperature)
	}
	return round(total / float64(len(thermostats)))
}

func round(num float64) int {
	if num < 0 {
		return int(math.Ceil(num - 0.5))
	}
	return int(math.Floor(num + 0.5))
}
