package main

import (
	"bitbucket.org/mbutterfield/birdcage/thermostat"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

func main() {
	var port uint
	result := flag.Uint("port", 0, "specify a port for this thermostat")
	nickname := flag.String("nickname", "living room", "specify the nickname of this thermostat")
	flag.Parse()
	rand.Seed(time.Now().UTC().UnixNano())
	if *result != 0 {
		port = *result
	} else {
		port = uint(rand.Intn(8100-8001) + 8001)
	}
	target_temp := uint(rand.Intn(76-65) + 65)
	current_temp := uint(rand.Intn(76-65) + 65)
	app := thermostat.NewApp(port, *nickname, target_temp, current_temp)
	fmt.Printf("listening on port %d\n", port)
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go runApp(app, wg)
	if err := registerThermostat(app.Thermostat); err != nil {
		panic(err)
	}
	wg.Wait()
}

func runApp(app *thermostat.App, wg *sync.WaitGroup) {
	if err := app.Run(); err != nil {
		panic(err)
	}
	wg.Done()
}

func registerThermostat(t *thermostat.Thermostat) error {
	result, err := json.Marshal(t)
	if err != nil {
		return err
	}
	resp, err := http.Post("http://localhost:3000/thermostat", "application/json", bytes.NewBuffer(result))
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}
